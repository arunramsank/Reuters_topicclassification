import datetime
import random
import sys
import numpy as np
import os
import yaml
import math
import random
import argparse
import numpy as np
import scipy as sc
from scipy import stats
import matplotlib.pyplot as plt
from collections import Counter
from collections import defaultdict
from scipy.special import softmax


def get_disagreement(scores, unlabeled=None, least=True):
    """[summary]

    Args:
        scores ([type]): [description]
        unlabeled ([type], optional): [description]. Defaults to None.
        least (bool, optional): [description]. Defaults to True.

    Returns:
        [type]: [description]
    """
    if unlabeled is None:
        unlabeled = range(len(scores[0]))
    classes = [np.argmax(score, axis=1) for score in scores]
    class_conf = [np.max(softmax(score, axis=1), axis=1) for score in scores]
    class_conf_mean = np.mean(class_conf, axis=0)
    disagreement = [len(set(x)) - 1 for x in list(zip(*classes))]
    d = list(zip(range(len(disagreement)), disagreement, class_conf_mean))
    random.shuffle(d)
    if least:
        d.sort(key=lambda x: (-x[1], x[2]))
    else:
        d.sort(key=lambda x: -x[1])
    if unlabeled is None:
        return [c[0] for c in d]
    else:
        print("Top disagreements:", d[:10], "\n\n")
        unlabeled_set = set(unlabeled)
        return [c[0] for c in d if c[0] in unlabeled_set]


def basic_select(meta_data, n_rec, portion="lowest"):
    """ Function to select indices based on meta data

    Given the meta data of the unlabeled samples, this
    function select the indices with either the highest 
    or the lowest portion

    Parameters:
    -----------
    meta_data: dict(int,float)
        dictionary such that keys are the indices of the 
        unlabeled data and values are the correponding 
        meta data (confidence, entropy, margin)

    n_rec: int
        Number of records to select
    portion: str, 'highest' or 'lower'
        If :portion='highest', then samples with the hightest
        meta data are selected. Likewise, if :portion='lowest',
        then samples with lowest meta data are selected
    """
    ls = [(k, meta_data[k]) for k in meta_data]
    ls.sort(key=lambda x: x[1])
    if portion == "highest":
        selected = ls[-n_rec:]
    elif portion == "lowest":
        selected = ls[:n_rec]
    else:
        raise ValueError("portion must be highest or lowest, got {}".format(portion))
    return list(map(lambda x: x[0], selected))


def percentile_select(meta_data, above, below, n_rec=None):
    """ Select samples based on the percentile of the meta data 

    Given the meta data of the unlabeled samples, this
    function select the samples whose meta data lie 
    in the percentile [:above, :below]

    Parameters:
    -----------
    meta_data: dict(int,float)
        dictionary such that keys are the indices of the 
        unlabeled data and values are the correponding 
        meta data (confidence, entropy, margin)

    above: float,
        lower bound of the percentile 
    
    below: float,
        upper bound of the percentile

    n_rec: int, default=None
        upper bound of the number of samples to select.
        If it is None, then all samples with meta data in
        [:above, :below] are selected
    """
    values = list(meta_data.values())
    lower_bound = np.percentile(values, above)
    upper_bound = np.percentile(values, below)

    selected = []
    for idx in meta_data:
        m = meta_data[idx]
        if m >= lower_bound and m <= upper_bound:
            selected.append(idx)

    if n_rec and n_rec < len(selected):
        return random.sample(selected, n_rec)
    else:
        return selected


def value_select(meta_data, above, below, n_rec=None):
    """ Select samples meta data on the value of the meta data 

    Given the meta data of the unlabeled samples, this
    function select the samples whose meta data lie 
    in the interval [:above, :below]

    Parameters:
    -----------
    meta_data: dict(int,float)
        dictionary such that keys are the indices of the 
        unlabeled data and values are the correponding 
        meta data (confidence, entropy, margin)

    above: float,
        lower bound of the interval 
    
    below: float,
        upper bound of the interval

    n_rec: int, default=None
        upper bound of the number of samples to select.
        If it is None, then all samples with meta data in
        [:above, :below] are selected
    """

    selected = []
    for idx in meta_data:
        m = meta_data[idx]
        if m >= above and m <= below:
            selected.append(idx)
    if n_rec and n_rec < len(selected):
        return random.sample(selected, n_rec)
    else:
        return selected


class Adaptiveconfidence(object):
    # def __init__(self,config):
    #     self.config = config

    def normSamp(self, scores):
        """
        Treats scores as a normal distribution and returns lower and upper bounds
        """
        lB = np.mean(scores) - (1.5 * np.std(scores))
        uB = np.mean(scores) + (1.5 * np.std(scores))
        return lB, uB

    def skewSamp(self, scores, skewscore):
        """
        Uses skewscore as a factor to sample
        
        """
        binCounts = Counter(list(np.round(scores, 2)))
        frequentConf = binCounts.most_common(1)[0][0]  #### Peak confidence
        lessfrequentConf = binCounts.most_common()[-1][0]
        if skewscore > 1:  #### High right skew
            skewscore = 0.95  #### cutoff higher positive scores
        elif skewscore < -1:  #### High left skew
            skewscore = -0.95  #### cutoff higher negative scores
        if 0 < skewscore < 0.95:  #### Right skewed
            # Set cutoffs on either side of peak
            lB = (1 - np.abs(skewscore)) * frequentConf
            uB = ((1 - np.abs(skewscore)) * frequentConf) + frequentConf

        elif -0.95 < skewscore < 0:  #### Left skewed
            lB = np.abs(skewscore) * lessfrequentConf
            uB = frequentConf
        elif skewscore == 0.95:  #### High Right skew
            lB = 0
            uB = np.percentile(scores, 50)
        else:  #### High left skew
            lB = np.percentile(scores, 25)
            uB = np.percentile(scores, 75)

        """
        #Uncomment Visualiztions when needed
        plt.hist(scores)
        plt.show()
        """
        return lB, uB

    def distributionTest(self, scores):
        """
        Analyze distribution of confidence scores of detections
        Input
        records -  list of lists containing recordIx and np array of classprobabilities for each record
        Outputs
        lB, uB  - lower and upper bounds to stream from based on confidence

        """

        skewscore = sc.stats.skew(scores, bias=True)
        skewscore = np.round(skewscore, 1)
        if skewscore != 0:  ### > 0 Right skewed < 0 Left skewed
            lB, uB = self.skewSamp(scores, skewscore)
        else:  ### == 0 Normal distribution
            lB, uB = self.normSamp(scores)
        return lB, uB

    def calcuncertainityscore(self, recsprobmap, lB, uB):
        """
        
        Returns record level uncertainity
        
        """
        for rec in recsprobmap:
            totalobjects = len(rec[1])
            numuncertainobjects = [
                1
                for probmap in rec[1]
                if lB[probmap[0]] <= probmap[1] <= uB[probmap[0]]
            ]
            uncertainintyscore = float(np.sum(numuncertainobjects) / totalobjects)
            rec.append(float(uncertainintyscore))
        return recsprobmap

    def stratifiedadaptivedistributionconfidence(self, records):
        """
        Adaptive confidence sampling for Multi class Object detection
        
        Input
        records    -  list of lists containing recordIx and list of np array of 
                      classprobabilities for each record   
                      [[recordix1 ,[np.array[(p1,p2,p3 ..)],np.array[(p1,p2,p3 ..)]]],
                       [recordix2 ,[np.array[(p1,p2,p3 ..)],np.array[(p1,p2,p3 ..)]]],
        
        stratified -  Bool, that decides whether the sampling should be 
                      stratified or not currently supports only non stratified 
                      (Stratified assumption is made on the inferred 
                      probabilities of classes and not actual labels) 
        
        Outputs
        batch - output batch of unlabeled Indices that need to priritized

        """

        scores = defaultdict(list)
        recprobsmap = []

        for rec in records:
            if not rec[1]:
                continue
            probs = rec[1]
            classscoremap = []

            for boxprobs in probs:
                boxclass = np.argmax(boxprobs)
                boxprob = boxprobs[boxclass]
                classscoremap.append([boxclass, boxprob])
                scores[boxclass].append(boxprob)
            recprobsmap.append([rec[0], classscoremap])

        ###### Perform distribution test on current batch of unlabeled #####
        lB, uB = {}, {}
        for k, v in scores.items():
            lB[k], uB[k] = self.distributionTest(v)

        uncertainitymap = self.calcuncertainityscore(recprobsmap, lB, uB)
        uncertainitymap.sort(
            key=lambda x: x[-1], reverse=True
        )  ###### Pool top records based on pooling criterion

        return uncertainitymap
