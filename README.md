# Topic Classifciation with BERT for the Reuters data set

## Installation

This repo is tested with python 3.6 on Ubuntu 18.04 on an AWS p2x.large instance. It is based on the implementation at [github.com/castorini/hedwig](https://github.com/castorini/hedwig).

### Get the Repo

Clone this repo with
```
git clone git@gitlab.com:arunramsank/Reuters_topicclassification.git
cd Reuters_topicclassification
```

### Set up Virtual Environment
Use your favorite tool to set up your virtual environment. For example use

```
python3 -m venv env
source env/bin/activate
```

### Setting up Pytorch
Install PyTorch with
```
pip install torch==1.5.0+cu92 torchvision==0.6.0+cu92 -f https://download.pytorch.org/whl/torch_stable.html
```

### Install Other Requirements
Install the remaining requirements with
```
pip install -r requirements.txt
```

## Training
To start the active learning experimentation, run 

Run
```
python AL.py
```
This scripts executes the main model training file for various different choices of parameters which represent different active learning strategies. 

### Training Options
The most important training parameters are 
```
strat: lowest, highest, outeroff, BALD, ... 
stepsize: 1, 2, 5
```
`strat` determines the active learning strategy to be used and `stepsize` determines the amount of labeled data (as a percentage of the full dataset) that should be added in each active learning loop. 

There are also other parameters which can be fine-tuned such as `maxsteps`, `epochs`, `lr`, `subset_seed` and more. However, for the Reuters dataset, the default values work well. 

### Training Procedure
All training results are saved in the [`results`](results/) folder. In the [`eval`](results/eval) folder, the evaluation results after each AL step are stored. In the [`test`](results/test) folder, the pre-softmax scores of the most recently trained model evaluated on the training set a stored. These values are used to chose the next datapoints to be labeled. Note that these outputs do not include labels (as expected in an active learning context).

## Data Analysis
The [`data-analysis`](data-analysis/) folder contains notebooks that help to explore and understand the Reuters news dataset as well as notebooks that visualize and compared the results of the active learning runs. 

## Trained Models
All fully trained models are saved under [`results/chkpts`](results/chkpts/).
