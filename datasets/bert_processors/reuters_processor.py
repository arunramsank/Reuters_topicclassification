import os
from typing import List
from datasets.bert_processors.abstract_processor import BertProcessor, InputExample


class ReutersProcessor(BertProcessor):
    NAME = "Reuters"
    NUM_CLASSES = 20
    IS_MULTILABEL = False

    def get_train_examples(
        self, data_dir, selection: List[int] = None,
    ):
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "Reuters", "train.tsv")),
            "train",
            selection=selection,
        )

    def get_dev_examples(
        self, data_dir, selection: List[int] = None,
    ):
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "Reuters", "dev.tsv")),
            "dev",
            selection=selection,
        )

    def get_test_examples(
        self, data_dir, selection: List[int] = None,
    ):
        return self._create_examples(
            self._read_tsv(os.path.join(data_dir, "Reuters", "test.tsv")),
            "test",
            selection=selection,
        )

    def _create_examples(
        self, lines, set_type, selection: List[int] = None,
    ):
        examples = []
        for (i, line) in enumerate(lines):
            if i == 0:
                continue
            guid = "%s-%s" % (set_type, i)
            text_a = line[1]
            label = line[0]
            if self.NUM_CLASSES is None:
                self.NUM_CLASSES = len(label)
            examples.append(
                InputExample(guid=guid, text_a=text_a, text_b=None, label=label)
            )
        if selection is None:
            print(f"Selecting {len(examples)} for {set_type}.\n")
            return examples
        else:
            print(
                f"Selecting {len(selection)} out of {len(examples)} for {set_type}.\n"
            )
            return [examples[i] for i in selection]
